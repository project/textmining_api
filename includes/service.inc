<?php

/**
 * @file
 * Service classes.
 */

/**
 * Process service abstract class for Text mining API.
 */
abstract class TextminingApiServiceAbstract extends ProcessApiServiceAbstract implements TextminingApiServiceInterface {
  
  public function threadConfigurationForm(ProcessApiThread $thread, array $form, array &$form_state) {
    
    $vocab_options = array('' => t('None')) + textmining_api_get_term_fields($thread->entity_type?$thread->entity_type:'node');
    
    $form = array(
      'field' => array(
        '#type' => 'select',
        '#title' => t('Vocabulary'),
        '#description' => t('Terms will be added in this vocabulary (field).'),
        '#options' => $vocab_options,
        '#default_value' => isset($thread->options['custom']['field']) ? $thread->options['custom']['field'] : NULL,
        '#required' => TRUE,
      ),
      'add_terms_mode' => array(
        '#type' => 'select',
        '#title' => t('Add terms mode'),
        '#description' => t('The way Text Mining API will add terms.'),
        '#options' => array('empty' => t('Only if no existing terms'), 'append' => t('Append new terms'), 'reset' => t('Reset before adding')),
        '#default_value' => isset($thread->options['custom']['add_terms_mode']) ? $thread->options['custom']['add_terms_mode'] : 'empty',
      ),
      'allow_create_terms' => array(
        '#type' => 'checkbox',
        '#title' => t('Allow create terms'),
        '#description' => t('Terms will be created if needed.'),
        '#default_value' => isset($thread->options['custom']['allow_create_terms']) ? $thread->options['custom']['allow_create_terms'] : TRUE,
      ),      
      
    );
    
    return $form;
  }  

  public function threadProcessItem(ProcessApiThread $thread, $id, array $item, &$context) {
    $add_terms_mode = isset($thread->options['custom']['add_terms_mode']) ? $thread->options['custom']['add_terms_mode'] : 'empty';
    $allow_create_terms = isset($thread->options['custom']['allow_create_terms']) ? $thread->options['custom']['allow_create_terms'] : TRUE;
    $item_entity=entity_load($thread->entity_type, array($id));
    $item_entity=reset($item_entity);
    
    if (!$item_entity) {
      watchdog('texmining_api', 'Cannot load entity !entity with !id', array('!entity' => $thread->entity_type, '!id' => $id), WATCHDOG_ERROR);
      return FALSE;
    }
    
    $terms = $this->threadGenerateItemTerms($thread, $id, $item);
    
    if (!is_array($terms)) {
      // FALSE means errors, if no terms should return an empty array.
      return FALSE;
    }
    
    if (!is_array(reset($terms))) {
     
      // we have just a single list of terms
      if ( isset($thread->options['custom']['field']) ) {
        $terms = array( $thread->options['custom']['field'] => $terms );
      }
      else {
        watchdog('texmining_api', 'Cannot determinate field to populate for thread !thread', array('!thread' => $thread->machine_name), WATCHDOG_ERROR);
        return FALSE;
      }
    }
    
    $modified=FALSE;
    
    foreach ($terms as $field => $field_terms) {
      if (!isset($field_terms['terms'])) {
        $field_terms = array('terms' => $field_terms);
      }
      if (!isset($field_terms['add_terms_mode'])) {
        $field_terms['add_terms_mode'] = isset($thread->options['custom']['add_terms_mode']) ? $thread->options['custom']['add_terms_mode'] : 'empty';
      }
      if (!isset($field_terms['allow_create_terms'])) {
        $field_terms['allow_create_terms'] = isset($thread->options['custom']['allow_create_terms']) ? $thread->options['custom']['allow_create_terms'] : FALSE;
      }

      if (isset($item_entity->{$field})) {
        
        $field_info = field_info_field($field);
        if (field_is_translatable($thread->entity_type, $field_info)) {
          $available_languages=field_available_languages($thread->entity_type, $field);
          $langcode = language_default('language');
          if (!in_array($langcode, $available_languages)) {
            // fallback to undefined
            $langcode = LANGUAGE_NONE;
          }
        }
        else {
          $langcode = LANGUAGE_NONE;
        }

        switch ($field_terms['add_terms_mode']) {
          case 'reset':
            $item_entity->{$field}[$langcode]=array();
            break;
          case 'append':
            //nothing
            break;
          default: //empty
            if (isset($item_entity->{$field}[$langcode])&&count($item_entity->{$field}[$langcode])) {
              //nothing
              continue;
            }
        }
        
        $vocabulary=taxonomy_vocabulary_machine_name_load($field_info['settings']['allowed_values'][0]['vocabulary']);
        foreach (array_unique($field_terms['terms']) as $name) {
          $terms=taxonomy_term_load_multiple(array(), array('name' => trim($name), 'vid' => $vocabulary->vid));
          $term=reset($terms);
          if ((!$term)&&$field_terms['allow_create_terms']) {
            $term=(object)array(
                'vid' => $vocabulary->vid,
                'name' => $name,
              );
            taxonomy_term_save($term);
          }
          
          if ($term) {
            $modified=TRUE;
            $item_entity->{$field}[$langcode][]=array('tid' => $term->tid);
          }
        }
      }
    }
    
    if ($modified) {
      entity_save($thread->entity_type, $item_entity);
    }
    
    return TRUE;
  }
  
  /**
   * By default we handle only nodes.
   * @see ProcessApiServiceAbstract::getSupportedEntityTypes()
   */
  public function getSupportedEntityTypes() {
    // arbitrary supports only nodes
    return array('node' => entity_get_info('node'));
  }
}

/**
 * Process service interface for Text mining API.
 */
interface TextminingApiServiceInterface {
  
  /**
   * Return the terms for an entity.
   * @param ProcessApiThread $thread
   * @param integer $id
   * @param array $item
   * 
   * @return array|FALSE
   *   The generated terms or FALSE if errors (will be re-processed)
   * 
   * for lonely term ref field :
   *   array(term1, term2, ...)
   *   
   * for multiple term ref fields and specific options:
   *   array(
   *     field1 => array('terms' => array(term1, term2, ...), 'add_terms_mode' => 'empty'|'append'|'reset', 'allow_create_terms' => TRUE|FALSE),
   *     field2 => array(term1, term2, ...),
   *     ...
   *   )
   */
  public function threadGenerateItemTerms(ProcessApiThread $thread, $id, array $item);
}