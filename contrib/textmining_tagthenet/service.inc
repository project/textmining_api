<?php

/**
 * @file
 * Service classes.
 */

/**
 * Text Minig service class for Tagthe.net Terms.
 */
class TextminingApiServiceTagthenet extends TextminingApiServiceAbstract {
  
  public function tagthenetOptionTypes() {
    return array(
      'topic' => t('Topic'),
      'title' => t('Title'),
      'person' => t('Person'),
      'language' => t('Language'),
      'author' => t('Author')
    );
  }
  
  public function threadConfigurationForm(ProcessApiThread $thread, array $form, array &$form_state) {
    $form = parent::threadConfigurationForm($thread, $form, $form_state);
    
    //unset default config
    unset($form['field']);
    unset($form['add_terms_mode']);
    unset($form['allow_create_terms']);
    
    if (!$thread->entity_type) return $form;
    
    $vocab_options = array('' => t('None')) + textmining_api_get_term_fields($thread->entity_type?$thread->entity_type:'node');
    
    $first=TRUE;
    foreach ($this->tagthenetOptionTypes() as $type => $type_name) {
      if (!$first) {
        $form['sep:' . $type] = array(
          '#markup' => '<hr>',
        );
      }
      $form += array(
        'field:' . $type => array(
          '#type' => 'select',
          '#title' => t('Vocabulary for @type', array('@type' => $type_name)),
          '#description' => t('Terms will be added in this vocabulary (field).'),
          '#options' => $vocab_options,
          '#default_value' => isset($thread->options['custom']['field:' . $type]) ? $thread->options['custom']['field:' . $type] : NULL,
        ),
        'add_terms_mode:' . $type => array(
          '#type' => 'select',
          '#title' => t('Add terms mode for @type', array('@type' => $type_name)),
          '#description' => t('The way Text Mining API will add terms.'),
          '#options' => array('empty' => t('Only if no existing terms'), 'append' => t('Append new terms'), 'reset' => t('Reset before adding')),
          '#default_value' => isset($thread->options['custom']['add_terms_mode:' . $type]) ? $thread->options['custom']['add_terms_mode:' . $type] : 'empty',
        ),
        'allow_create_terms:' . $type => array(
          '#type' => 'checkbox',
          '#title' => t('Allow create terms for @type', array('@type' => $type_name)),
          '#description' => t('Terms will be created if needed.'),
          '#default_value' => isset($thread->options['custom']['allow_create_terms:' . $type]) ? $thread->options['custom']['allow_create_terms:' . $type] : FALSE,
        ),      
      
      );
      $first=FALSE;
    }
        
    return $form;
  }  
  
  public function threadGenerateItemTerms(ProcessApiThread $thread, $id, array $item) {
    
    $text='';
    foreach ($item as $field) {
        if (!is_string($field['value'])) continue;
       $text .= ' ' . $field['value'];
    }

    $options = array(
      'data' => 'text=' . urlencode($text),
      'method' => 'POST',
    );
  
    $response = textmining_api_http_request(TEXTMINING_TAGTHENET_URL, $options);
    if ($response->code != 200) {
      watchdog('autotagging_tagthenet', 'Received response code !code from Tagthe.net', array('!code' => $response->code), WATCHDOG_ERROR);
      return FALSE;
    }
    
    $dom = DOMDocument::loadXML($response->data);
    $xp = new DOMXPath($dom);
    $terms=array();
    
    $dims = $xp->evaluate("/memes/meme/dim");
    for ($i = 0; $i < $dims->length; $i++) {
      $dim = $dims->item($i);
      $type = $dim->getAttribute("type");
      
      if (isset($thread->options['custom']['field:' . $type])&&$thread->options['custom']['field:' . $type]) {
        $field = $thread->options['custom']['field:' . $type];
        if (!isset($terms[$field]['add_terms_mode'])) $terms[$field]['add_terms_mode'] = NULL;
        $this->combineOption(
          'add_terms_mode',
          $terms[$field]['add_terms_mode'],
          isset($thread->options['custom']['add_terms_mode:' . $type]) ? $thread->options['custom']['add_terms_mode:' . $type] : 'empty'
        );
        if (!isset($terms[$field]['allow_create_terms'])) $terms[$field]['allow_create_terms'] = NULL;
        $this->combineOption(
          'allow_create_terms',
          $terms[$field]['allow_create_terms'],
          isset($thread->options['custom']['allow_create_terms:' . $type]) ? $thread->options['custom']['allow_create_terms:' . $type] : FALSE
        );
        $items = $xp->evaluate("item", $dim);
        for ($j = 0; $j < $items->length; $j++) {
          $dim_item = $items->item($j);
          $terms[$field]['terms'][] = $dim_item->nodeValue;
        }
      }
    }
    return $terms;
  }
  
  protected function combineOption($name, &$current, $new) {
    if ($current===NULL) {
      $current=$new;
      return;   
    }
    if ($current==$new) return;
    switch ($name) {
      case 'add_terms_mode':
        if ($current=='empty'||$new=='empty') {
          $current='empty';
          return;
        }
        if ($current=='append'||$new=='append')
        {
          $current='append';
          return;
        }
        $current='reset';
        return;
      case 'allow_create_terms':
        $current=$current&&$new;
        return;
    }
  }
  
}
