<?php

/**
 * @file
 * Service classes.
 */

/**
 * Text Minig service class for OpenCalais Terms.
 */
class TextminingApiServiceOpenCalais extends TextminingApiServiceAbstract {

  public function openCalaisOptionTypes() {
    // From http://www.opencalais.com/documentation/calais-web-service-api/calais-semantic-metadata-french/entities-index-and-definitions-
    $res=array();
    foreach(array(
        'City',
        'Company',
        'Continent',
        'Country',
        //'Currency',
        //'EmailAddress',
        //'FaxNumber',
        //'MarketIndex',
        'NaturalFeature',
        'Organization',
        'Person',
        //'PhoneNumber',
        'ProvinceOrState',
        'Region',
        //'URL',
      ) as $tag) {
      $res[$tag]=t($tag);
    }
    
    return $res;
  }
  
  public function configurationForm(array $form, array &$form_state) {
    $form=parent::configurationForm($form, $form_state);
    
    $form += array(
      'opencalais_apikey' => array(
        '#type' => 'textfield',
        '#title' => t('OpenCalais API Key'),
        '#default_value' => isset($this->server->options['custom']['opencalais_apikey']) ? $this->server->options['custom']['opencalais_apikey'] : NULL,
        '#required' => TRUE,
      ),
    );
        
    return $form;
  }

  public function threadConfigurationForm(ProcessApiThread $thread, array $form, array &$form_state) {
    $form = parent::threadConfigurationForm($thread, $form, $form_state);
    
    if (!$thread->entity_type) return $form;
    
    $vocab_options = array('' => t('None')) + textmining_api_get_term_fields($thread->entity_type?$thread->entity_type:'node');
    
    foreach ($this->openCalaisOptionTypes() as $type => $type_name) {
      $form['sep:' . $type] = array(
        '#markup' => '<hr>',
      );

      $form += array(
        'field:' . $type => array(
          '#type' => 'select',
          '#title' => t('Vocabulary for @type', array('@type' => $type_name)),
          '#description' => t('Terms will be added in this vocabulary (field).'),
          '#options' => $vocab_options,
          '#default_value' => isset($thread->options['custom']['field:' . $type]) ? $thread->options['custom']['field:' . $type] : NULL,
        ),
        'add_terms_mode:' . $type => array(
          '#type' => 'select',
          '#title' => t('Add terms mode for @type', array('@type' => $type_name)),
          '#description' => t('The way Text Mining API will add terms.'),
          '#options' => array('empty' => t('Only if no existing terms'), 'append' => t('Append new terms'), 'reset' => t('Reset before adding')),
          '#default_value' => isset($thread->options['custom']['add_terms_mode:' . $type]) ? $thread->options['custom']['add_terms_mode:' . $type] : 'empty',
        ),
        'allow_create_terms:' . $type => array(
          '#type' => 'checkbox',
          '#title' => t('Allow create terms for @type', array('@type' => $type_name)),
          '#description' => t('Terms will be created if needed.'),
          '#default_value' => isset($thread->options['custom']['allow_create_terms:' . $type]) ? $thread->options['custom']['allow_create_terms:' . $type] : FALSE,
        ),      
      
      );
    }
        
    return $form;
  }  
  
  public function threadGenerateItemTerms(ProcessApiThread $thread, $id, array $item) {
    
    $text='';
    foreach ($item as $field) {
        if (!is_string($field['value'])) continue;
       $text .= ' ' . $field['value'];
    }

    $data = array();
    $xmlinput = '<c:params xmlns:c="http://s.opencalais.com/1/pred/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"><c:processingDirectives c:contentType="text/html" c:enableMetadataType="GenericRelations,SocialTags" c:outputFormat="Application/JSON"/></c:params>';
    $data['licenseID'] = $thread->server()->options['custom']['opencalais_apikey'];
    $data['content'] = $text;
    $data['paramsXML'] = $xmlinput;
      
    $options = array(
      'data' => $data,
      'method' => 'POST',
    );
    
    $response = textmining_api_http_request(TEXTMINING_OPENCALAIS_URL, $options);
    if ($response->code != 200) {
      watchdog('texmining_opencalais', 'Received response code !code from OpenCalais', array('!code' => $response->code), WATCHDOG_ERROR);
      return FALSE;
    }
    
    $terms=array();
    
    $tags=json_decode($response->data, TRUE);
    if (is_array($tags))
    {
      foreach(json_decode($response->data, TRUE) as $tag)
      {
        if (!isset($tag['name'])) continue;
        $field=FALSE;
        if (isset($tag['_type'])) {
          // specific tag type
          $type=$tag['_type'];
          if (isset($thread->options['custom']['field:' . $type])&&$thread->options['custom']['field:' . $type]) {
            $field = $thread->options['custom']['field:' . $type];
            if (!isset($terms[$field]['add_terms_mode'])) $terms[$field]['add_terms_mode'] = NULL;
            $this->combineOption(
            	'add_terms_mode',
              $terms[$field]['add_terms_mode'],
              isset($thread->options['custom']['add_terms_mode:' . $type]) ? $thread->options['custom']['add_terms_mode:' . $type] : 'empty'
            );
            if (!isset($terms[$field]['allow_create_terms'])) $terms[$field]['allow_create_terms'] = NULL;
            $this->combineOption(
              'allow_create_terms',
              $terms[$field]['allow_create_terms'],
              isset($thread->options['custom']['allow_create_terms:' . $type]) ? $thread->options['custom']['allow_create_terms:' . $type] : FALSE
            );
          }
        }
        if (!$field) {
          //take default values
          if (isset($thread->options['custom']['field'])&&$thread->options['custom']['field']) {
            $field = $thread->options['custom']['field'];
            if (!isset($terms[$field]['add_terms_mode'])) $terms[$field]['add_terms_mode'] = NULL;
            $this->combineOption(
            	'add_terms_mode',
              $terms[$field]['add_terms_mode'],
              isset($thread->options['custom']['add_terms_mode']) ? $thread->options['custom']['add_terms_mode'] : 'empty'
            );
            if (!isset($terms[$field]['allow_create_terms'])) $terms[$field]['allow_create_terms'] = NULL;
            $this->combineOption(
              'allow_create_terms',
              $terms[$field]['allow_create_terms'],
              isset($thread->options['custom']['allow_create_terms']) ? $thread->options['custom']['allow_create_terms'] : FALSE
            );
          }
        }
        if ($field) {
          $terms[$field]['terms'][] = $tag['name'];
        }
      }
    }
    return $terms;
  }
  
  protected function combineOption($name, &$current, $new) {
    if ($current===NULL) {
      $current=$new;
      return;   
    }
    if ($current==$new) return;
    switch ($name) {
      case 'add_terms_mode':
        if ($current=='empty'||$new=='empty') {
          $current='empty';
          return;
        }
        if ($current=='append'||$new=='append')
        {
          $current='append';
          return;
        }
        $current='reset';
        return;
      case 'allow_create_terms':
        $current=$current&&$new;
        return;
    }
  }
}
